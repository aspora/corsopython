#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 10 14:05:18 2022

@author: angelo
"""

# Scrivere una funzione che prende in input
# una lista e rimuove gli elementi consecutivi
 # uguali
# [1,1,1,2,3,3,4,1,1,3,3,'a','a'] -> [1,2,3,4,1,3,'a']


def togli_consecutivi(lista):
    offset = 0
    for i in range(len(lista)-1):
        if lista[i-offset] == lista[i-offset+1]:
            #lista.pop(i)
            del lista[i-offset]
            offset += 1
    return lista

print(togli_consecutivi([1,1,1,2,3,3,4,1,1,3,3,'a','a']))


def togli_consecutivi1(lista):
    for i in range(len(lista)-1, 0, -1):
        if lista[i] == lista[i-1]:
            #lista.pop(i)
            del lista[i]
    return lista

def togli_consecutivi2(lista):
    for i, v in enumerate(lista):
        if lista[i] == lista[i+1]:
            #lista.pop(i)
            del lista[i]
    return lista