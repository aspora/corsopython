#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 10 09:50:55 2022

@author: angelo
"""

def pulisci(riga, caratteri):
    for carattere in caratteri:
        riga = riga.replace(carattere, " ")
    return riga




def conta_finali(file):
    d = dict.fromkeys('aeiou', 0)

    with open(file) as f:
        caratteri = set(f.read())
        accentate = 'àéèìòóù'
        alfabeto = accentate +''.join([chr(i) for i in range(ord('a'), ord('z')+1)])
        alfabeto = alfabeto + alfabeto.upper()
        caratteri_da_eliminare = caratteri - set(alfabeto)
        table = dict.fromkeys(map(ord, caratteri_da_eliminare), ord(" "))
        table.update({ord('è'):ord('e'), ord('é'):ord('e'), ord('ì'):ord('i'), ord('ù'):ord('u'), ord('à'):ord('a'), ord('ò'):ord('o'), ord('ó'):ord('o')})
        f.seek(0)
        for riga in f:
            riga = riga.translate(table)
            for parola in riga.split():
                if parola[-1] in d:
                    d[parola[-1]] += 1
    return d

print(conta_finali('esercizi/6personaggi.txt'))



def parole_finale_accentate(file):
    accentate = 'àéèìòóù'
    alfabeto = accentate +''.join([chr(i) for i in range(ord('a'), ord('z')+1)])
    alfabeto = alfabeto + alfabeto.upper()

    caratteri_da_eliminare = set([chr(i) for i in range(0,255)])
    caratteri_da_eliminare.difference_update(set(alfabeto))

    table = dict.fromkeys(map(ord, caratteri_da_eliminare), ord(" "))

    d = {l:set() for l in accentate}
    with open(file) as f:
        for riga in f:
            for parola in riga.translate(table).split():
                if parola[-1].lower() in accentate:
                    d[parola[-1].lower()].add(parola)
    return d