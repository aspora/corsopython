# -*- coding: utf-8 -*-



# Scrivere una funzione che prende in input un
# file di testo e calcola per ogni vocale
# il numero di parole che finiscono per quella
# vocale.
# La funzione ritorna un dizionario
# in cui ogni chiave è una vocale e il valore
# corrispondente è il numero di parole che
# terminano con quella vocale. Se una parola
# non termina per una vocale, non va contata.
# Prestare attenzione ai segni di interpunzione
# (ovvero virgole, punti, punti e virgola etc.).
# Osservare il file 6personaggi.txt




def Esercizio2(filename):

    dizionario={'a':0, 'e':0  ,'i':0, 'o':0, 'u':0}
    tuttoaltro=[]

    with open(filename) as t:


        for linea in t:

            miastringa=toglicaratteristrani(linea)
            # print (miastringa)
            for parole in miastringa.split():
                # print (parole[-1])
                if (parole[-1]) in dizionario:
                   dizionario[parole[-1]]+=1
                # else:
                #     if (parole[-1]) not in tuttoaltro:
                #         tuttoaltro+=[(parole[-1])]



    return (dizionario,tuttoaltro)




def toglicaratteristrani(stringa):

        nuovastringa=""
        dizcaratterispeciali=',;£?_!ç-."ï»¿'
        for carattere in stringa.strip():
            if carattere not in dizcaratterispeciali:
                nuovastringa+=carattere
            else:
                nuovastringa+=" "
        return nuovastringa










