#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  7 14:06:19 2022

@author: angelo
"""

x = input('Dammi un numero ')
if x[0] in '+-':
    i = 1
else:
    i = 0
i = 1 if x[0] in '+-' else 0

if x[i:].isnumeric() or (x.count('.') == 1  and
               x[i:x.index('.')].isnumeric() and
               x[x.index('.')+1:].isnumeric()):
    if i == 1:
        r = float(x[0:i] +str(float(x[i:])**(1/3)))
    else:
        r = float(x)**(1/3)
    print(f'La radice cubica è {r:.4f}')
else:
    print('Errore. Non hai inserito un numero.')
