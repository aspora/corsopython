#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  8 16:37:50 2022

@author: angelo
"""

# Scrivere una funzione che prende una stringa fatta
# di parole separate da spazi, e ritorna un dizionario
# in cui c'è una chiave per ogni lettera presente
# nella stringa, e come valore ci sono le
# parole della stringa che contengono quella lettera

#'oca cocacola' -> {'o':['oca', 'cocacola'], 'c':['oca', 'cocacola'], 'a':['oca', 'cocacola'], 'l':['cocacola']}

def liste_parole(stringa):
    d = {}
    for parola in stringa.split():
        for carattere in parola:
