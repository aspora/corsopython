# -*- coding: utf-8 -*-
"""
Created on Wed Feb  9 14:19:48 2022

@author: garage211

Scrivere una funzione che prende in input un
file di testo e calcola per ogni vocale
il numero di parole che finiscono per quella
vocale.
La funzione ritorna un dizionario
in cui ogni chiave è una vocale e il valore
corrispondente è il numero di parole che
terminano con quella vocale. Se una parola
non termina per una vocale, non va contata.
Prestare attenzione ai segni di interpunzione
(ovvero virgole, punti, punti e virgola etc.).
Osservare il file 6personaggi.txt
"""
filetesto = '6personaggi.txt'

def contavocali(file):
    diz = {'a':0,'e':0,'i':0,'o':0,'u':0,'.':0}
    with open (file,'r') as f:
        for righe in f:
            for parola in righe.split():
                if parola.isalpha():
                    #print(parole)
                        if parola[-1] in diz:
                            diz[parola[-1]]+=1
    return diz

#diz ['positivi'] = [numero for numero in lista if numero >=0 ]