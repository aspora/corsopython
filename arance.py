#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 10 16:04:22 2022

@author: angelo

Abbiamo una cassetta di arance
che è una lista di liste.
Se c'è  0: il posto è vuoto.
Se c'è 1: c'è un'arancia.
Se c'è 2: c'è un'arancia marcia.

Dopo un minuto, ogni arancia vicino
ad un'arancia marcia diventa marcia
a sua volta. (Vicina: a sinistra o
a destra, sopra o sotto).

Scrivere una funzione che prende
la cassetta di arance e ritorna
il numero di minuti che servono
per far marcire tutte le arance.
Se non può accadere, la funzione
ritorna -1.

"""

arance = [[1, 1, 1, 1],
[1, 1, 1, 1],
[1, 1, 1, 1],
[1, 0, 1, 2]]

def fai_marcire(arance):
    arance2 = [a.copy() for a in arance]
    for i in range(len(arance)):
        for j in range(len(arance[i])):
            if arance[i][j] == 2:
                if j>0 and arance[i][j-1] == 1:
                    arance2[i][j-1]=2
                if j<len(arance[i])-1 and arance[i][j+1] == 1:
                    arance2[i][j+1]=2
                if i>0 and arance[i-1][j] == 1:
                    arance2[i-1][j]=2
                if i<len(arance)-1 and arance[i+1][j] == 1:
                    arance2[i+1][j]=2
    return arance2

def fai_marcire2(arance):
    arance2 = [a.copy() for a in arance]
    for i in range(len(arance)):
        for j in range(len(arance[i])):
            if arance[i][j] == 1:
                if 2 in arance_vicine(arance, i, j):
                    arance2[i][j] = 2
    return arance2


def verifica_marce(arance):
    for riga in arance:
        if 1 in riga:
            return False
    return True

def arance_vicine(arance, i, j):
    vicini = []
    if j>0:
        vicini.append(arance[i][j-1])
    if j<len(arance[i])-1:
        vicini.append(arance[i][j+1])
    if i>0:
        vicini.append(arance[i-1][j])
    if i<len(arance)-1:
        vicini.append(arance[i+1][j])
    return vicini

def verifica_highlander(arance):
    for i in range(len(arance)):
        for j in range(len(arance[i])):
            if sum(arance_vicine(arance, i, j)) == 0:
                return True
    return False

def conta_minuti(arance):
    if verifica_highlander(arance):
        return -1
    minuti = 0
    while not verifica_marce(arance):
        minuti+=1
        arance2 = fai_marcire2(arance)
        if arance2 == arance:
            return -1
        arance = arance2
    return minuti
print(conta_minuti(arance))
arance2 = [
[1, 0, 1, 1],
[1, 0, 1, 1],
[1, 0, 0, 1],
[1, 0, 1, 2]]