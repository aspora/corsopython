#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  8 15:40:07 2022

@author: angelo
"""

# scrivere una funzione che prende in ingresso una
# stringa e conta le occorrenze per ogni lettera
# ritornando un dizionario

# casa -> {'c':1, 'a':2, 's':1}

def conta_frequenze(parola):
    diz = {}
    for car in parola:
        if car not in diz:
            diz[car] = parola.count(car)
    return diz


def conta_frequenze2(parola):
    diz = {}
    for car in parola:
        if car not in diz:
            diz[car] = 1
        else:
            diz[car] += 1
    return diz


def conta_frequenze3(parola):
    diz = {}
    for car in parola:
        diz[car] = diz.get(car, 0) +1
    return diz


def conta_frequenze4(parola):
    diz = {}
    for car in parola:
        try:
            diz[car] +=1
        except KeyError:
            diz[car] = 1
    return diz