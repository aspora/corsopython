#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  7 15:40:13 2022

@author: angelo
"""

# Scrivere una funzione che prende un numero e mi
# dice se è numero primo o no: ritorna True se si, False altrimenti

import math

def e_numero_primo(n):
    for i in range(2, int(math.sqrt(n))+1):
        if n % i == 0:
            return False
    return True


def divisori(n):
    lista = [1, n]
    for i in range(2, int(math.sqrt(n))+1):
        if n % i == 0:
            lista += [i, n//i]
#            lista += [i]
#            if n//i not in lista:
#                # if n//i != i:
#                lista += [n//i]
    if n // math.sqrt(n) == math.sqrt(n):
        lista = lista[:-1]
    return sorted(lista)

