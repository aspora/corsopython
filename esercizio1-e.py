# -*- coding: utf-8 -*-


# Scrivere una funzione che riceva in input una lista di numeri float e
# restituisca un dizionario con due chiavi 'positivi' e 'negativi',
# dove i valori sono le liste dei numeri positivi e negativi della lista in input.



def Esercizio1(numeri):
    dizionario = {}

    dizionario['positivi']=[numero for numero in numeri if numero>=0]
    dizionario['negativi']=[numero for numero in numeri if numero<0]

    return (dizionario)







def Esercizio1Ordine(numeri,ordine):
    dizionario = {}

    ordine = True if ordine != 0 else False

    dizionario['positivi']=[numero for numero in sorted(numeri, reverse=ordine) if numero>=0]
    dizionario['negativi']=[numero for numero in sorted(numeri, reverse=ordine) if numero<0]



    return (dizionario)



def Esercizio1FOR(numeri):
    dizionario = {'positivi':[],'negativi':[]}


    for numero in numeri:
        if numero>=0:
            dizionario['positivi']+=[numero]
        else:
            dizionario['negativi']+=[numero]
    return dizionario





    return (dizionario)


