#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  7 15:05:39 2022

@author: angelo
"""

def valore(x, y=1):
    if x %2:
        return (y*x)**2
    else:
        return y*x

