#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 10 15:15:03 2022

@author: angelo
"""

from Punto import Punto

class Poligono():
    def __init__(self, spigoli):
        if type(spigoli) == list and
        all([isinstance(s, Punto) for s in spigoli]):
            self.spigoli = spigoli

class Quadrato():
    def __init__(self, p, lato):
        self.p = p
        self.lato = lato

    def perimetro(self):
        return 4 * self.lato

    def area (self):
        return self.lato * self.lato

class Rettangolo():
    def __init__(self, p, lato):
        self.p = p
        self.lato = lato


