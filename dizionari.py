#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  8 14:37:58 2022

@author: angelo
"""

d = {}   # dizionario vuoto
d = dict()   # dizionario vuoto

d = {'a':1, 'b':2, 'c':3} # dizionario esplicito
d = dict(a=1, b=2, c=3)   # equivalente

d['d'] = 4 # aggiunta elemento

d.get('d') # accesso elemento
d['k'] # Errore chiave non presente
d.get('k') # Non errore, ma None, chiave non presente

d.pop('d')# rimozione elemento con ritorno del valore
d.popitem() # rimozione casuale con ritorno della coppia chiave valore
del d['a'] # rimozione con errore se assente

len(d)     # elementi in d
d.keys()   # chiavi di d
d.values() # valori di d
d.items()  # coppia chiavi valori di d

d.copy() ## Copia "shallow"
import copy
copy.deepcopy(d) ## Copia "ricorsiva"

e = {'d':5, 'e':19}
d.update(e)
