# -*- coding: utf-8 -*-
"""
Created on Wed Feb  9 14:06:30 2022

@author: garage211

Scrivere una funzione che riceva in input una lista di numeri float e
restituisca un dizionario con due chiavi 'positivi' e 'negativi', dove
i valori sono le liste dei numeri positivi e negativi della lista in input.

"""
l = [1.2,2.2,2.3,3.4,4.5,-3,-6.5]

l2 = [1,2,3,4,5,6,7,8,9]

def esercizio1(lista):
    diz = { }
    diz ['positivi'] = [numero for numero in lista if numero >=0 ]
    diz ['negativi'] = [numero for numero in lista if numero < 0]
    return diz

def esercizio1_bis(lista):
    diz = {'positivi':[],'negativi':[]}
    for numero in lista:
        if numero >= 0:
            diz ['positivi'] += [numero]
        else:
            diz ['negativi'] += [numero]
    return diz