#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 11 11:46:13 2022

@author: angelo

Scrivere una funzione che prende il
nome di una directory e il nome di un
utente e trova tutti i file di proprietà
dell'utente ricorsivamente.
E ritorna un dizionario in cui
la chiave è il percorso completo del
file e il valore è lo stat di quel file.
"""

import os

def trova_file_owner(percorso, utente):
    diz = {}
    for file in os.listdir(percorso):
        filename = percorso + '/' + file
        if os.path.isfile(filename):
            s = os.stat(filename)
            if s.st_uid==utente:
                diz[filename] = s
        if os.path.isdir(filename):
            try:
                os.listdir(filename)
                diz.update(trova_file_owner(filename, utente))
#                diz2 = trova_file_owner(filename, utente)
#                for chiave, valore in diz2.items():
#                    diz[chiave] = valore
            except PermissionError:
                continue
    return diz

import json
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("path", help='path da cui iniziare la ricerca')
    parser.add_argument("user", help='utente di cui fare la ricerca', type=int)
    parser.add_argument('-f', '--filename', help='file json in cui salvare il risultato')
    args = parser.parse_args()

    diz = trova_file_owner(args.path, args.user)
    if args.filename:
        with open(args.filename, 'w') as f:
            json.dump(diz, f)
    else:
        print(json.dumps(diz))


