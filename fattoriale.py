#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  8 12:42:50 2022

@author: angelo
"""

# 5! = 5* 4!

def fattoriale(n):
    if n == 1:
        return 1
    return n * fattoriale(n-1)


def fattoriale2(n):
    f= 1
    while n >= 1:
        f *= n
        n-=1
    return f