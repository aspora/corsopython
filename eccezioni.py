#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 10 15:52:39 2022

@author: angelo
"""


l= [1,2,3,4]
try:
    l [55] = 4
except ValueError:
    print('Ho beccato un ValueError')
except (IndexError, KeyError) as e:
    print('Ho beccato un IndexError', e)
else:
    print('Ci è andata bene')
finally:
    print('E ho detto tutto')