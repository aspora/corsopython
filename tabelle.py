#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  9 09:30:30 2022

@author: angelo
Gestire una tabella con i seguenti campi:
    progressivo
    anno
    oggetto
    quantità
    importo unitario
"""
o1 = {'progressivo':5, 'anno':2021, 'oggetto':'qualcosa', 'quantita':10, 'importou':300}

o2 = {'progressivo':6, 'anno':2021, 'oggetto':'qualcosa di nuovo', 'quantita':5, 'importou':200}

l = [o1, o2]

l.append({'progressivo':1, 'anno':2020, 'oggetto':'qualcosa di vecchio', 'quantita':50, 'importou':20})
l.append({'progressivo':3, 'anno':2020, 'oggetto':'qualcosa di vecchio', 'quantita':5, 'importou':40})
l.append({'progressivo':3, 'anno':2021, 'oggetto':'qualcosa di strano', 'quantita':5, 'importou':40})
l.append({'progressivo':13, 'anno':2021, 'oggetto':'qualcosa di anomalo', 'quantita':15, 'importou':20})

s = "{:>4}|{:>4}|{:<30}|{:>3}|{:>10}|{:>10}"
for o in sorted(l, key= lambda o: (o['progressivo'],o['anno'])):
    print(s.format(o['progressivo'], o['anno'], o['oggetto'][:30], o['quantita'], o['importou'], o['quantita']*o['importou']))