#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  7 15:28:05 2022

@author: angelo
"""
import math

def equazione(a,b,c):
    blocco = math.sqrt(b**2 - 4*a*c)
    radice1 = (-b + blocco)/(2*a)
    radice2 = (-b - blocco)/(2*a)
    return radice1, radice2