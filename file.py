#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  9 11:06:44 2022

@author: angelo
Scriviamo una funzione che prende una lista
di stringhe e  il nome di un file
le scrive nel file in input ordinate
in ordine alfabetico, una riga per una
"""
l1 = ['cocco', 'kiwi', 'mela', 'banana', 'ananas', 'a.kiwi']


def scrivi_lista_file(lista, filename):
    #if os.path.isfile(filename)
    with open(filename, 'w') as f:
        for elemento in sorted(lista, key=len):
            #scrivi elemento in filename
            f.write(elemento+'\n')
            #print(elemento, file=f)


def print_lista_file(lista, filename):
    with open(filename, 'w') as f:
        print("\n".join(sorted(lista, key=len)), file=f)
#        f.write("\n".join(sorted(lista, key=len)))
