#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  8 11:59:22 2022

@author: angelo
"""

# Funzione che prende in input una stringa di parole separate da spazi
# e restituisce una lista con le lunghezze delle parole della stringa.

s = 'banane lampone    kiwi      cocomero ananas mirtillo pera mela'
[6, 7, 4, 8, 6, 8, 4, 4]

def lettere_stringa(stringa):
    l = []
    for parola in stringa.split():
        l += [len(parola)]
    return l



# scrivere una funzione che prende una stringa di parole separate
# da spazi e un carattere e ritorna una lista con il numero di
# volte che quel carattere compare in ognua delle parole.
s = 'banane lampone    kiwi      cocomero ananas mirtillo pera mela'
carattere = 'a'
[2, 1, 0, 0, 3, 0, 1, 1]
def conta_lettere_in_stringa(stringa, carattere):
    l = []
    for parola in stringa.split():
        l += [parola.count(carattere)]
    return l

