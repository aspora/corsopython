#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  7 14:06:19 2022

@author: angelo
"""

x = input('Dammi un numero ')
if x[0] in '+-':
    i = 1
else:
    i = 0

while not( x[i:].isnumeric() or (x.count('.') == 1  and
               x[i:x.index('.')].isnumeric() and
               x[x.index('.')+1:].isnumeric())):
    print('Errore. Non hai inserito un numero.')
    x = input('Dammi un numero ')
    if x[0] in '+-':
        i = 1
    else:
        i = 0

if i == 1:
    print('La radice cubica è', float(x[0:i] +str(float(x[i:])**(1/3))))
else:
    print('La radice cubica è', float(x)**(1/3))