#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  9 12:16:43 2022

@author: angelo
"""

def from_file_to_diz(filename):
    with open(filename) as f:
        diz = {}
        for linea in f:
            chiave, valore = linea.split()
            diz[chiave] = float(valore)
    return diz

def from_file_to_diz2(filename):
    with open(filename) as f:
        return {l.split()[0]:float(l.split()[1]) for l in f}