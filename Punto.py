#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 10 14:44:53 2022

@author: angelo
"""

class Punto():
    x = 0
    y = 0
    z = 0
    """ Punto dell'asse cartesiano """
    def __init__(self, x=0, y=0):
        """ Costruttore, parametro x e y """
        if isinstance(x, (int, float)) and isinstance(y, (int, float)):
            self.x = x
            self.y = y
        else:
            raise TypeError("Il Punto ha solo x e y numerici")

    def simm_x(self):
        return Punto(self.x, -self.y)

    def simm_y(self):
        return Punto(-self.x, self.y)

    def simm_xy(self):
        return Punto(-self.x, -self.y)

    def __repr__(self):
        ''' Nell'interprete '''
        return 'Oggetto Punto({},{})'.format(self.x, self.y)

    def __str__(self):
        ''' Nella print '''
        return 'Punto({},{})'.format(self.x, self.y)

    def dist(self, p):
        if type(p) != Punto:
            raise TypeError("La distanza è solo tra due tipi Punto")
        return ((self.x - p.x)**2 + (self.y - p.y)**2) ** 0.5

    def add(self, p):
        if not isinstance(p, Punto):
            raise TypeError("La somma è solo tra due tipi Punto")
        return Punto(self.x+p.x, self.y+p.y)

    def __add__(self, p):
        return self.add(p)
