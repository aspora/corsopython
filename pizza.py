#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  7 12:23:06 2022

@author: angelo
"""

nome = input('Come ti chiami? ')
pizza = input('Che pizza ordini? ')
pezzi = input('Quanti pezzi?')

if pezzi.isnumeric():
    pezzi = int(pezzi)
else:
    print('Non è un numero: te ne do 2')
    pezzi = 2

pezzo = 10

#print('Caro ' + nome + ' la tua pizza '+pizza+' è stata ordinata.')

print('Caro', nome, 'la tua pizza', pizza, 'è stata ordinata e sarà pronta fra', pezzo * pezzi, 'secondi')





