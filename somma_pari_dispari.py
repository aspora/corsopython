#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  8 11:27:23 2022

@author: angelo
"""

# Una funzione ch prende una lista di interi e restituisce la somma di tutti i
# numeri pari meno la somma di tutti i numeri dispari che la compongono.

l = [4,7,1,45,2,33,88,24,67,89]

def somma_pari_dispari(lista):
    p = 0
    d = 0
    for i in lista:
        if i%2:
            d+=i
        else:
            p+=1
    return p-d


def somma_pari_dispari2(lista):
    somma = 0
    for i in lista:
        if i%2:
            somma-=i
        else:
            somma+=1
    return somma


def somma_pari_dispari3(lista):
    somma = 0
    for i in lista:
        somma += (-1)**(i%2) * i
    return somma


def somma_pari_dispari4(lista):
    lista_pari = [i for i in lista if i%2==0]
    lista_dispari = [i for i in lista if i%2!=0]
    return sum(lista_pari) - sum(lista_dispari)


def somma_pari_dispari5(lista):
    def segno(i):
        return (-1)**(i%2)
    return sum([segno(i)*i for i in lista])