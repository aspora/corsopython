#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  7 16:24:43 2022

@author: angelo
"""

# Scrivere una funzione che prende tre interi
# g, m, a e ritorna true se è una data valida
# false altrimenti


def annobisestile(a):
    f = False
    if a % 400 == 0 or a==0:
        f = True
    elif a % 100 == 0:
        f = False
    elif a % 4 == 0:
        f= True
    return f


def annobisestile2(a):
    if a % 400 == 0 or a==0:
        return True
    elif a % 100 == 0:
        return False
    elif a % 4 == 0:
        return True
    return False


def data_valida(g, m, a):
    giorni = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    if annobisestile(a):
        giorni[2] = 29
    if 1 <= m <= 12 and 1 <= g <= giorni[m]:
        return True
    return False