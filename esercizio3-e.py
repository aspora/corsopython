# -*- coding: utf-8 -*-
"""
Scrivere una funzione che prende in input una lista di elementi
e il nome di un file.
La funzione salva nel file il cui nome è in input ogni elemento
della lista e il numero di volte che questo è ripetuto nella
lista. Ogni elemento è salvato una sola volta e il numero di
ripetizioni è salvato separato da un carattere tab.
L'ordine in cui compare ogni elemento nel file è lo stesso
della lista in input.

salva_ripetizioni([1,1,1,7,7,3,3,3.4,3.4,3.5,'casa','a','a',1])
1\t4
7\t2
3\t2
3.4\t2
3.5\t1
casa\t1
a\t2

"""


def salva_ripetizioni(lista, nomefile="test.txt"):
    """ Senza dizionario
         lista : lista di elementi
         nomefile: dove salvare
    """
    with open(nomefile,'w') as f:
        elenco = set(lista)
        risultato=[]
        for elemento in lista:
            if elemento in elenco:
                print(elemento, '\t', lista.count(elemento), file=f)
                risultato+=[str(elemento)+'\t'+str(lista.count(elemento))]
                elenco.remove(elemento)
    return risultato
    # return (elenco)



def salva_ripetizioni1(lista,nomefile="test.txt"):
    """ Con dizionari """
    with open(nomefile,'w') as f:
        risultato={}
      # //  elenco = lista.split(',')
        for el in lista:
            if el in risultato:
                risultato[el]+=1
            else:
                risultato[el]=1
        for stringa in  (risultato):
          print(str(stringa)+'\t'+str(lista.count(stringa)), file=f)


    return (risultato)
    # return (elenco)
