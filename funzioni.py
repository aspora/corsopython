#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 10 11:56:51 2022

@author: angelo
"""
lista = []
def gasp(a, b=2, c = lista):
    for i in range(b):
        c.append(a)
    return c

def foo(a, b=5, c=6, d=1, *e, **kargs):
    print(e)
    print(kargs)
    return a + b +c+d


x = 5
def bar(y):
    z = 8
    x = 0
    print('locals:',locals())
    print('x', globals()['x'])
    print(x, y, z)
bar(6)
print(x)


def somma(e):
    print(id(e), e)
    e = e + e
    print(id(e), e)

def incremento(e):
    print(id(e), e)
    e += e
    print(id(e), e)

def estendi(e):
    print(id(e), e)
    e.extend(e)
    print(id(e), e)

def aggiungi(lista, pos, ogg):
    print(lista)
    lista[pos] = ogg
    print(lista)