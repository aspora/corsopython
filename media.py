#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  7 15:12:02 2022

@author: angelo
Scrivere una funzione che prende una lista di interi e ne calcola
e restituisce la media
"""


def media(una_lista):
    somma = 0
    for elemento in una_lista:
        somma = somma + elemento
    return somma/len(una_lista)
