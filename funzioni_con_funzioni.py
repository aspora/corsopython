#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  9 10:41:16 2022

@author: angelo
"""



l1 = ['cocco', 'kiwi', 'mela', 'banana', 'ananas', 'a.kiwi']
sorted(l1, key = len)
sorted(l1, key = (len,s))
sorted(l1, key = lambda s: (len,s))
sorted(l1, key = lambda s: (len(s),s))
sorted(l1, key = lambda s: (len(s),s), reverse=True)
sorted(l1, key = lambda s: (-len(s),s), reverse=True)
max(l1)
max(l1, key=len)
min(l1)
min(l1, key=len)

filter(str.isalpha, l1)
list(filter(str.isalpha, l1))
f = filter(str.isalpha, l1)
next(f)
next(f)
next(f)
next(f)


q = [1,6,3,5,7,7,3,2,2,3,5,6,7,12]
[i**2 for i in q]
list(map(lambda i:i**2, q))
list(filter(lambda i: i%2==0, q))
filter(lambda i: i%2==0, q)
next(f)
next(f)
next(f)